#!/bin/sh
# Note: this script uses `local` but is otherwise POSIX-compatible
# (see https://stackoverflow.com/a/18600920)

main() {
    # Find and switch to the folder this script is in
    local script_folder="$(dirname "$(readlink -f "${0}")")"
    cd "${script_folder}"
    
    addon="Crafting Villagers"
    behavior_pack="${addon} - Behavior Pack"
    resource_pack="${addon} - Resource Pack"

    mc_folder="/sdcard/games/com.mojang"
    phone_behavior_pack="${mc_folder}/development_behavior_packs/${behavior_pack}"
    phone_resource_pack="${mc_folder}/development_resource_packs/${resource_pack}"

    # Remove old versions
    adb shell "rm -rf '${phone_behavior_pack}' && rm -rf '${phone_resource_pack}'"

    adb push "../${behavior_pack}" "${mc_folder}/development_behavior_packs/"
    adb push "../${resource_pack}" "${mc_folder}/development_resource_packs/"
}

main "$@"