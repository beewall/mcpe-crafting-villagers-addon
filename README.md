# [Crafting Villagers][0]
Trading-based alternative to "More Recipes"-type addons for Minecraft Bedrock Edition.


- [Crafting Villagers](#crafting-villagers)
  - [Why?](#why)
  - [What does it add?](#what-does-it-add)
    - [Crafter](#crafter)
    - [Elder](#elder)
    - [Mysterious Trader](#mysterious-trader)
  - [Spawner Villagers](#spawner-villagers)
  - [Build](#build)
    - [Prep](#prep)
    - [Actual Building](#actual-building)
    - [Dev Install](#dev-install)

## Why?
Because addons that modify recipes require "Experimental Gameplay" on (at least for now), which is incompatible with Realms.

However, adding trades does NOT, eaning this addon is useable on Realms (if you're not using Realms, then use a proper recipes addon, not this).

## What does it add?
It adds two villagers (Crafter and Elder), and a rare version of the Wandering Trader (the Mysterious Trader).

See [Trades.md][1] for details.

### Crafter
The Crafter has a few useful trades, like:
* Logs to paper
* Rotten flesh to leather
* Saddles
* Horse armor
* Bells
* Nametags

### Elder
The Elder trades for unobtainable "removed" blocks, like the:
* Camera (not fully functional, just flashes and disappears)
* Nether Reactor Core (not functional)
* Old Stonecutter (you guessed it, not functional)
* Petrified Oak Slab (updated texture!... I'm not an artist so I just layered effects on top of oak planks lol)
* Glowing Obsidian (updated texture!... just obsidian with a red overlay)

### Mysterious Trader
The Mysterious Trader trades a few "cursedish" blocks, like:
* Water source blocks
* Lava source blocks
* Fire blocks
* Nether Portal blocks (unfortunately, these only face east/west)
* Mob Spawner

(Most of these will break if they get a block update)

<!-- References -->
[1]: Trades.md

## Spawner Villagers
There are three spawner villagers: hostile, peaceful, fish. Each trades spawn eggs for their category of mob. Each trade is 64 emeralds plus 64 of an item related to the mob.

## Build
### Prep
Make sure to follow everything in `DEVELOPMENT.md`.

### Actual Building
"Building" this is pretty simple, as there's no real code.

(Note: "RP" refers to the `Crafting Villagers - Resource Pack` folder, and `BP` to the `Crafting Villagers - Behavior Pack` folder.)
1. Zip `RP`'s contents.
2. Change the file extension to `.mcpack`.
3. Repeat Steps 1-2 but for `BP`.
4. Zip *those* up into one zip.
5. Change the extension to `.mcaddon`.

### Dev Install
A Bash script is provided that will push the addon to the addon development folders on a connected Android device using ADB (and a VS Code task is provided to run this as well).

<!-- References -->
[0]: https://gitlab.com/beewall/mcpe-crafting-villagers-addon