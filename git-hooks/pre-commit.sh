#! /bin/sh
# Note: this script uses `local` but is otherwise POSIX-compatible
# (see https://stackoverflow.com/a/18600920)

main() {
    # Find and switch to the folder this script is in
    local script_folder="$(dirname "$(readlink -f "${0}")")"
    cd "${script_folder}"

    # (Theoretically) the project's root folder
    local project_root='..'

    # If in .git/hooks rather than git-hooks, then root is actually 2 above
    if contains "${script_folder}" '.git/hooks'; then
        project_root='../..'
    fi
    # Get a list of changed files
    local files_modified="$(git diff --cached --name-status)"
    local files_added=''
    local files_removed=''
    local files_changed=''

    # Each line in files_modified starts with one of these
    local prefix_added='A       '
    local prefix_removed='D       '
    local prefix_changed='M       '

    # Loop over files to see how they were modified (added, removed, changed)
    echo "${files_modified}" | while read file; do
        case "${file}" in
        "${prefix_added}"*)
            files_added="${files_added}\n${file}"
            ;;
        "${prefix_removed}"*)
            files_added="${files_removed}\n${file}"
            ;;
        "${prefix_changed}"*)
            files_added="${files_changed}\n${file}"
            ;;
        esac
    done

    # Check if any of the changed files include villager trades files
    if contains "${files_modified}" '_trades.json'; then
        echo "${files_modified}"
        # Generate Trades.md from the modified files
        sh "${project_root}/scripts/generate-trades-doc.sh"

        # Add the changed Trades.md file
        cd "${project_root}"
        git add "Trades.md"
        cd "${script_folder}"
    fi
}

## Checks if the first parameter contains the second
contains() {
    case "${1}" in
    *"${2}"*)
        true
        ;;
    *)
        false
        ;;
    esac
}
main "$@"
