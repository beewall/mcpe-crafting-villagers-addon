#! /bin/sh
# Note: this script uses `local` but is otherwise POSIX-compatible
# (see https://stackoverflow.com/a/18600920)

main() {
    # Find and switch to the folder this script is in
    local script_folder="$(dirname "$(readlink -f "${0}")")"
    cd "${script_folder}"

    # Name of this addon and folder
    local addon="Crafting Villagers"

    # Folder containing trade JSONs
    local trades_folder="../${addon} - Behavior Pack/trading/economy_trades"

    # File to put trade info into
    local trades_doc="../Trades.md"

    # Prefix for lines documenting trades
    local trade_prefix="// Trade: "

    local toc_placeholder='[[_TOC_]]'

    # Top-level heading
    echo "# ${addon} - Trades" >"${trades_doc}"
    echo >>"${trades_doc}"

    # Put marker for where to put Table of Contents
    echo "${toc_placeholder}" >>"${trades_doc}"
    echo >>"${trades_doc}"

    # Loop over the various trade JSONs
    for file in "${trades_folder}"/*_trades.json; do
        echo "Reading '${file}'..."
        # We assume filename is villager_name_trades.json, so take it from that
        trades_name=$(basename "${file}" "_trades.json")
        villager_name=$(to_titlecase "$(echo "${trades_name}" | sed "s/_/ /g")")

        # Put it as a second-level heading
        echo "## ${villager_name}" >>"${trades_doc}"

        read_trades "${file}" "${trade_prefix}" >>"${trades_doc}"
    done

    echo 'Reading trades done.'
    echo 'Creating table of contents...'

    # Replace newline in table with | (newlines break sed)
    local table_of_contents="$(gen_toc "${trades_doc}" | tr '\n' '|')"

    local sed_command="s/$(sed_escape ${toc_placeholder})/${table_of_contents}/g"

    # Replace any instances of placeholder in the doc with a table of contents
    sed -i "${sed_command}" "${trades_doc}"

    # Bring the newlines back!
    sed -i 's/|/\
/g' "${trades_doc}"
}

## Reads from a file to find Minecraft trades documented in the comment format
## `<Wanted Item> [+ Second Wanted Item] -> <Given Item>`
##
## @param trades_file       File to read trades from
## @param trade_prefix      Prefix for lines documenting a trade (default: '// Trade: ')
read_trades() {
    local trades_file="${1}"
    local trade_prefix="${2:-'// Trade: '}"

    while read line; do
        if starts_with "${line}" "${trade_prefix}"; then
            # Remove the prefix
            trade="$(echo "${line}" | sed "s/$(sed_escape "${trade_prefix}")//")"
            # Find the result of the trade
            local result="$(echo "${trade}" | sed -n 's/^.* -> \(.*\)$/\1/p')"
            echo "### ${result}"

            # Find the first wanted item (either `wanted +` or `wanted ->`)
            local wanted_one="$(echo "${trade}" | sed -n 's/^\([^+]*\) .*->.*$/\1/p')"

            # Put the wanted item (no newline)
            printf "%s" "${wanted_one}"

            # Check if comment contains a + (means it contains two wants)
            if contains "${trade}" "+"; then
                # Find the second wanted item (`+ wanted2`)
                local wanted_two="$(echo "${trade}" | sed -n 's/^.* [+] \(.*\) ->.*$/\1/p')"

                # Put the second wanted item
                printf "%s" " + ${wanted_two}"
            fi

            # Put the result
            echo " -> ${result}"

            # Put a blank line
            echo
        fi
    done <"${trades_file}"
}

## Creates a TOC from a Markdown file and sends it to stdout
## Does NOT support setext-style headings
## Heading links approximately follow the GitLab style:
## https://gitlab.com/help/user/markdown#header-ids-and-links)
gen_toc() {
    local trades_file="${1}"

    while read line; do
        if starts_with "${line}" "#"; then
            # Gets the heading ID (or what could possibly be it, depends on host site)
            local slug="$(slugify "${line}")"

            # Gets just the # at the start of a heading
            local level_markers="$(echo "${line}" | sed -n 's/^\(#*\) .*$/\1/p')"

            # Counts how many # there were
            local heading_level=$(echo "${line}" | awk -F "#" '{print NF-1}')

            # Removes a few specific characters and leading spaces
            local chars_to_remove="[][#'\"]"
            local heading="$(echo "${line}" |
                sed "s/$chars_to_remove//g" |
                sed 's/^ //')"

            # Make a link from heading and slug
            local entry="[${heading}](${slug})"

            # Add spaces to nest list to appropriate level
            # (Loop from https://unix.stackexchange.com/a/410637)
            local i=1
            while [ "$i" -ne "${heading_level}" ]; do
                # Add 4 spaces per heading level after the first
                entry="    ${entry}"
                i=$((i + 1))
            done

            echo "${entry}"
            echo "Added heading ${line}" >&2
        fi
    done <"${trades_file}"
}

## Capitalizes the first letter of every word in the string
to_titlecase() {
    # (Command from https://stackoverflow.com/a/31972726)
    echo ${1} | awk '{for (i=1;i<=NF;i++) $i=toupper(substr($i,1,1)) substr($i,2)} 1'
}

## Checks if the first parameter contains the second
contains() {
    case "${1}" in
    *"${2}"*)
        true
        ;;
    *)
        false
        ;;
    esac
}

## Checks if the first parameter starts with the second
starts_with() {
    case "${1}" in
    "${2}"*)
        true
        ;;
    *)
        false
        ;;
    esac
}

## Escapes sed's special characters (basic regex mode only)
sed_escape() {
    echo "${1}" | sed 's/\([$*.[+/\^]\)/\\\1/g'
}

## A very simple slugify function
## Replaces spaces with dashes, trips some punctuation, and lowercases words
## Not tested with non-ASCII characters
slugify() {
    local string="${1}"
    # A regex to match a bunch of punctuation
    local punct_regex="[][,.\!?<>;:'\"{}()@#$%^&*\`~+=|\/\\]"
    local slug="$(
        echo ${1} |
            # Replace spaces with hyphens
            sed 's/ /-/g' |
            # Remove punctuation
            sed "s/${punct_regex}//g" |
            # Replace multiple hyphens with one
            tr -s '-' '-' |
            # Remove hyphen at start of string
            sed 's/^-//' |
            # Remove hyphen at end of string
            sed 's/-$//' |
            # Convert to lowercase
            tr '[:upper:]' '[:lower:]'
    )"
    echo "${slug}"
}

main "$@"
