#!/bin/sh
# Note: this script uses `local` but is otherwise POSIX-compatible
# (see https://stackoverflow.com/a/18600920)

main() {
    # Find and switch to the folder this script is in
    local script_folder="$(dirname "$(readlink -f "${0}")")"
    cd "${script_folder}"

    # Switch to the folder the script is in
    cd "${script_folder}"

    # Create the hooks folder
    mkdir -p '../.git/hooks'

    # Symlink the files to the hooks folder
    for file in '../git-hooks/'*'.sh'; do
        # Switch to the hooks folder
        cd '../git-hooks/'

        # Remove the path part from the filename
        local filename="${file##*/}"
        local link="../.git/hooks/${filename%%.sh}"

        if [ "${OSTYPE}" = 'msys' ]; then
        # If on Windows, copy instead (symlinking requires admin)
            cp "${filename}" "${link}"
        else
            # Remove file extension and make symlink
            ln -s "${filename}" "${link}"
        fi

        # Switch back to scripts folder
        cd "${script_folder}"
    done
}

main "$@"
