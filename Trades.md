# Crafting Villagers - Trades

[Crafting Villagers - Trades](crafting-villagers-trades)
    [Crafter](crafter)
        [4 String](4-string)
        [Iron Horse Armor](iron-horse-armor)
        [Gold Horse Armor](gold-horse-armor)
        [Diamond Horse Armor](diamond-horse-armor)
        [Elytra](elytra)
        [Name Tag](name-tag)
        [16 Paper](16-paper)
        [16 Paper](16-paper)
        [Saddle](saddle)
        [Leather](leather)
        [Ice](ice)
        [Bell](bell)
    [Elder](elder)
        [Old Stonecutter](old-stonecutter)
        [Stonecutter](stonecutter)
        [Glowing Obsidian](glowing-obsidian)
        [Petrified Oak Slab](petrified-oak-slab)
        [Nether Reactor Core](nether-reactor-core)
        [Enchanted Golden Apple](enchanted-golden-apple)
        [Camera](camera)
        [Portfolio](portfolio)
    [Fish Spawner](fish-spawner)
        [Pufferfish Spawn Egg](pufferfish-spawn-egg)
        [Salmon Spawn Egg](salmon-spawn-egg)
        [Tropical Fish Spawn Egg](tropical-fish-spawn-egg)
        [Cod Spawn Egg](cod-spawn-egg)
    [Hostile Spawner](hostile-spawner)
        [Zombie Spawn Egg](zombie-spawn-egg)
        [Creeper Spawn Egg](creeper-spawn-egg)
        [Skeleton Spawn Egg](skeleton-spawn-egg)
        [Spider Spawn Egg](spider-spawn-egg)
        [Zombie Pigman Spawn Egg](zombie-pigman-spawn-egg)
        [Slime Spawn Egg](slime-spawn-egg)
        [Enderman Spawn Egg](enderman-spawn-egg)
        [Silverfish Spawn Egg](silverfish-spawn-egg)
        [Cave Spider Spawn Egg](cave-spider-spawn-egg)
        [Ghast Spawn Egg](ghast-spawn-egg)
        [Magma Cube Spawn Egg](magma-cube-spawn-egg)
        [Blaze Spawn Egg](blaze-spawn-egg)
        [Zombie Villager Spawn Egg](zombie-villager-spawn-egg)
        [Witch Spawn Egg](witch-spawn-egg)
        [Stray Spawn Egg](stray-spawn-egg)
        [Husk Spawn Egg](husk-spawn-egg)
        [Wither Skeleton Spawn Egg](wither-skeleton-spawn-egg)
        [Guardian Spawn Egg](guardian-spawn-egg)
        [Elder Guardian Spawn Egg](elder-guardian-spawn-egg)
        [Shulker Spawn Egg](shulker-spawn-egg)
        [Endermite Spawn Egg](endermite-spawn-egg)
        [Vindicator Spawn Egg](vindicator-spawn-egg)
        [Phantom Spawn Egg](phantom-spawn-egg)
        [Ravager Spawn Egg](ravager-spawn-egg)
        [Evoker Spawn Egg](evoker-spawn-egg)
        [Vex Spawn Egg](vex-spawn-egg)
        [Drowned Spawn Egg](drowned-spawn-egg)
        [Pillager Spawn Egg](pillager-spawn-egg)
        [Polar Bear Spawn Egg](polar-bear-spawn-egg)
    [Mysterious Trader](mysterious-trader)
        [Water](water)
        [Lava](lava)
        [Fire](fire)
        [Nether Portal](nether-portal)
        [Mob Spawner](mob-spawner)
    [Peaceful Spawner](peaceful-spawner)
        [Chicken Spawn Egg](chicken-spawn-egg)
        [Cow Spawn Egg](cow-spawn-egg)
        [Pig Spawn Egg](pig-spawn-egg)
        [Sheep Spawn Egg](sheep-spawn-egg)
        [Wolf Spawn Egg](wolf-spawn-egg)
        [Villager Spawn Egg](villager-spawn-egg)
        [Mooshroom Spawn Egg](mooshroom-spawn-egg)
        [Squid Spawn Egg](squid-spawn-egg)
        [Rabbit Spawn Egg](rabbit-spawn-egg)
        [Bat Spawn Egg](bat-spawn-egg)
        [Iron Golem Spawn Egg](iron-golem-spawn-egg)
        [Snow Golem Spawn Egg](snow-golem-spawn-egg)
        [Ocelot Spawn Egg](ocelot-spawn-egg)
        [Horse Spawn Egg](horse-spawn-egg)
        [Donkey Spawn Egg](donkey-spawn-egg)
        [Mule Spawn Egg](mule-spawn-egg)
        [Skeleton Horse Spawn Egg](skeleton-horse-spawn-egg)
        [Zombie Horse Spawn Egg](zombie-horse-spawn-egg)
        [Llama Spawn Egg](llama-spawn-egg)
        [Parrot Spawn Egg](parrot-spawn-egg)
        [Dolphin Spawn Egg](dolphin-spawn-egg)
        [Turtle Spawn Egg](turtle-spawn-egg)
        [Cat Spawn Egg](cat-spawn-egg)
        [Panda Spawn Egg](panda-spawn-egg)
        [Wandering Trader Spawn Egg](wandering-trader-spawn-egg)
        [Fox Spawn Egg](fox-spawn-egg)
        [Bee Spawn Egg](bee-spawn-egg)


## Crafter
### 4 String
Wool -> 4 String

### Iron Horse Armor
7 Iron Ingots -> Iron Horse Armor

### Gold Horse Armor
7 Gold Ingots -> Gold Horse Armor

### Diamond Horse Armor
7 Diamonds -> Diamond Horse Armor

### Elytra
16 Phantom Membrane -> Elytra

### Name Tag
2 String + Paper -> Name Tag

### 16 Paper
Log -> 16 Paper

### 16 Paper
Other Type of Log -> 16 Paper

### Saddle
3 Leather + 2 Iron Nuggets -> Saddle

### Leather
Rotten Flesh -> Leather

### Ice
4 Snow Blocks -> Ice

### Bell
2 Gold Ingots + 2 Sticks -> Bell

## Elder
### Old Stonecutter
Stonecutter -> Old Stonecutter

### Stonecutter
Old Stonecutter -> Stonecutter

### Glowing Obsidian
Obsidian + 4 Glowstone Dust -> Glowing Obsidian

### Petrified Oak Slab
Oak Slab + Stone Slab -> Petrified Oak Slab

### Nether Reactor Core
6 Iron Ingots + 3 Diamonds -> Nether Reactor Core

### Enchanted Golden Apple
8 Gold Blocks + Apple -> Enchanted Golden Apple

### Camera
4 Redstone Blocks + Glowstone Dust -> Camera

### Portfolio
Book + Glowstone Dust -> Portfolio

## Fish Spawner
### Pufferfish Spawn Egg
64 Emeralds + 64 Pufferfish -> Pufferfish Spawn Egg

### Salmon Spawn Egg
64 Emeralds + 64 Raw Salmon -> Salmon Spawn Egg

### Tropical Fish Spawn Egg
64 Emeralds + 64 Tropical Fish -> Tropical Fish Spawn Egg

### Cod Spawn Egg
64 Emeralds + 64 Raw Cod -> Cod Spawn Egg

## Hostile Spawner
### Zombie Spawn Egg
64 Emeralds + 64 Rotten Flesh -> Zombie Spawn Egg

### Creeper Spawn Egg
64 Emeralds + 64 Gunpowder -> Creeper Spawn Egg

### Skeleton Spawn Egg
64 Emeralds + 64 Bones -> Skeleton Spawn Egg

### Spider Spawn Egg
64 Emeralds + 64 String -> Spider Spawn Egg

### Zombie Pigman Spawn Egg
64 Emeralds + 64 Gold Blocks -> Zombie Pigman Spawn Egg

### Slime Spawn Egg
64 Emeralds + 64 Slime Blocks -> Slime Spawn Egg

### Enderman Spawn Egg
64 Emeralds + 64 Ender Pearls -> Enderman Spawn Egg

### Silverfish Spawn Egg
64 Emeralds + 64 Mossy Cobblestone -> Silverfish Spawn Egg

### Cave Spider Spawn Egg
64 Emeralds + 64 Spider Eyes -> Cave Spider Spawn Egg

### Ghast Spawn Egg
64 Emeralds + 64 Ghast Tears -> Ghast Spawn Egg

### Magma Cube Spawn Egg
64 Emeralds + 64 Magma Cream -> Magma Cube Spawn Egg

### Blaze Spawn Egg
64 Emeralds + 64 Blaze Rods -> Blaze Spawn Egg

### Zombie Villager Spawn Egg
64 Emeralds + 64 Hay Blocks -> Zombie Villager Spawn Egg

### Witch Spawn Egg
64 Emeralds + 64 Glass Bottles -> Witch Spawn Egg

### Stray Spawn Egg
64 Emeralds + 64 Ice -> Stray Spawn Egg

### Husk Spawn Egg
64 Emeralds + 64 Sandstone -> Husk Spawn Egg

### Wither Skeleton Spawn Egg
64 Emeralds + 32 Wither Skeleton Skulls -> Wither Skeleton Spawn Egg

### Guardian Spawn Egg
64 Emeralds + 64 Prismarine Crystals -> Guardian Spawn Egg

### Elder Guardian Spawn Egg
64 Emeralds + 64 Dry Sponges -> Elder Guardian Spawn Egg

### Shulker Spawn Egg
64 Emeralds + 64 Shulker Shells -> Shulker Spawn Egg

### Endermite Spawn Egg
64 Emeralds + 64 End Stone -> Endermite Spawn Egg

### Vindicator Spawn Egg
64 Emeralds + 64 Iron Ingots -> Vindicator Spawn Egg

### Phantom Spawn Egg
64 Emeralds + 64 Phantom Membrane -> Phantom Spawn Egg

### Ravager Spawn Egg
64 Emeralds + 64 Diamonds -> Ravager Spawn Egg

### Evoker Spawn Egg
64 Emeralds + 64 Diamond Blocks -> Evoker Spawn Egg

### Vex Spawn Egg
64 Emeralds + 64 Iron Ore -> Vex Spawn Egg

### Drowned Spawn Egg
64 Emeralds + 64 Nautilus Shells -> Drowned Spawn Egg

### Pillager Spawn Egg
64 Emeralds + 64 Arrows -> Pillager Spawn Egg

### Polar Bear Spawn Egg
64 Emeralds + 64 Packed Ice -> Polar Bear Spawn Egg

## Mysterious Trader
### Water
Water Bucket -> Water

### Lava
Lava Bucket -> Lava

### Fire
Flint and Steel -> Fire

### Nether Portal
Obsidian + Fire -> Nether Portal

### Mob Spawner
8 Anvils + Nether Star -> Mob Spawner

## Peaceful Spawner
### Chicken Spawn Egg
64 Emeralds + 64 Feathers -> Chicken Spawn Egg

### Cow Spawn Egg
64 Emeralds + 64 Beef -> Cow Spawn Egg

### Pig Spawn Egg
64 Emeralds + 64 Porkchops -> Pig Spawn Egg

### Sheep Spawn Egg
64 Emeralds + 64 Wool -> Sheep Spawn Egg

### Wolf Spawn Egg
64 Emeralds + 64 Bone Blocks -> Wolf Spawn Egg

### Villager Spawn Egg
64 Emeralds + 64 Emerald Blocks -> Villager Spawn Egg

### Mooshroom Spawn Egg
64 Emeralds + 64 Red Mushrooms -> Mooshroom Spawn Egg

### Squid Spawn Egg
64 Emeralds + 64 Ink Sacs -> Squid Spawn Egg

### Rabbit Spawn Egg
64 Emeralds + 64 Rabbit Hide -> Rabbit Spawn Egg

### Bat Spawn Egg
64 Emeralds + 64 Stone -> Bat Spawn Egg

### Iron Golem Spawn Egg
64 Emeralds + 64 Iron Blocks -> Iron Golem Spawn Egg

### Snow Golem Spawn Egg
64 Emeralds + 64 Blue Ice -> Snow Golem Spawn Egg

### Ocelot Spawn Egg
64 Emeralds + 64 Cocoa Beans -> Ocelot Spawn Egg

### Horse Spawn Egg
64 Emeralds + 64 Leather -> Horse Spawn Egg

### Donkey Spawn Egg
64 Emeralds + 64 Chests -> Donkey Spawn Egg

### Mule Spawn Egg
64 Emeralds + 64 Trapped Chests -> Mule Spawn Egg

### Skeleton Horse Spawn Egg
Horse Spawn Egg + 64 Bones -> Skeleton Horse Spawn Egg

### Zombie Horse Spawn Egg
Horse Spawn Egg + 64 Rotten Flesh -> Zombie Horse Spawn Egg

### Llama Spawn Egg
64 Emeralds + 64 Carpets -> Llama Spawn Egg

### Parrot Spawn Egg
64 Emeralds + 64 Cookies -> Parrot Spawn Egg

### Dolphin Spawn Egg
64 Emeralds + 64 Cooked Salmon -> Dolphin Spawn Egg

### Turtle Spawn Egg
64 Emeralds + 64 Scutes -> Turtle Spawn Egg

### Cat Spawn Egg
64 Emeralds + 64 Cooked Cod -> Cat Spawn Egg

### Panda Spawn Egg
64 Emeralds + 64 Scaffolding -> Panda Spawn Egg

### Wandering Trader Spawn Egg
64 Emeralds + 64 Emeralds -> Wandering Trader Spawn Egg

### Fox Spawn Egg
64 Emeralds + 64 Sweet Berries -> Fox Spawn Egg

### Bee Spawn Egg
64 Emeralds + 64 Honey Blocks -> Bee Spawn Egg

