# [Development][0]
This file is a list of things to follow while doing things for this project.

(Some could be inferred from looking at the files already here, but I figured I'd write them down.)

## Table of Contents
- [Development](#development)
  - [Table of Contents](#table-of-contents)
  - [Before Developing](#before-developing)
  - [Recommendations](#recommendations)
  - [Trades](#trades)
  - [Textures](#textures)
  - [Markdown](#markdown)
    - [Headings](#headings)
    - [Links and Images](#links-and-images)
    - [ToC](#toc)
    - [Wrapping](#wrapping)
  - [Bash Scripts](#bash-scripts)
    - [POSIX Compatibility](#posix-compatibility)
    - [`main` Function](#main-function)

## Before Developing
Make sure to set the pre-commit hook in Git! The `scripts` folder contains a script to do this for you (on *nix, it links to `git-hooks/pre-commit.sh`, while on Windows it copies that file).

## Recommendations
If you use Windows, I'd recommend setting up Git-Bash or something similar, as most of the build scripts use Bash.

I'd recommend using VS Code, as it's what I use, but you can use anything, really.

If you use VS Code, I'd recommend using the [Markdown All-In-One extension][1] if you change any of the Markdown files, as it can auto-generate a Table of Contents (configure it to use the GitLab slugify mode).

[1]: https://github.com/yzhang-gh/vscode-markdown
     (Markdown All-In-One extensions GitHub)

## Trades
If you add a trade, please make sure to add a comment documenting it! These are in the format

```
// Trade: <Wanted Item One> [+ Wanted Item Two] -> <Given Item>
```

Note that, for simplicity of my parser's sake, this is *whitespace-sensitive*. There must be *exactly one* space between each of the items (whitespace *is* allowed within items, with no escaping needed).

Also note that these comments cannot contain the '|' character, due to a limitation in the way that the Table of Contents generator is implemented.

When you commit anything, the `Trades.md` file will be automatically generated from these comments (if you set the pre-commit hook right).

## Textures
If you edit a texture, please save it both in the appropriate textures subfolder (as a PNG), and in the appropriate `source_images` subfolder (as a file in whatever native format your editor uses, such as PDN for paint.net or PSD for PhotoShop). This allows it to be edited properly by other contributors, with things like layers intact, while also allowing the game to render it (as Minecraft only supports a few formats).

## Markdown
### Headings
The top-level heading should be at the top of the page, and be the page's title (or the project's title, in the case of a README). This heading should also be a reference link to the reference `[0]`.

Other headings should be of levels two through six (there should be no other top-level headings).

### Links and Images
Links images should be [reference-style][1], using numbers as the reference (`img-[number]` for images).

References should be defined at the bottom of the top _sub_section (second-level heading). Links should be defined first, under an HTML comment `<!-- References -->`, then images under an HTML comment `<!-- Images -->`. 

The reference definitions should have a relevant title defined using parentheses on the line after the rest of the definition, indented up to the reference URL.

The reference `[0]` should be defined to refer to the file on the Git repo hosting the project, and be defined at the bottom of the file.

### ToC
Markdown files should, if they contain sections, have a Table of Contents. This should be a nested list of links to headings and sub-headings. The links to the headings should follow [GitLab's heading ID generation rules][2]. 

Links in a table of contents should, as an exception to the reference link rule, use [inline links][3].

### Wrapping
Markdown files should be soft-wrapped, not hard-wrapped.

<!-- References -->
[1]: https://spec.commonmark.org/0.29/#reference-link
     ('Reference Links' in the CommonMark spec)
[2]: https://gitlab.com/help/user/markdown#header-ids-and-links
     (GitLab's heading ID generation rules)
[3]: https://spec.commonmark.org/0.29/#inline-link
     ('Inline Links' in the CommonMark spec)

## Bash Scripts
### POSIX Compatibility
Use local variables, but otherwise keep things POSIX-compatible. The first lines of the file should be

```
#!/bin/sh
# Note: this script uses `local` but is otherwise POSIX-compatible
# (see https://stackoverflow.com/a/18600920)
```

(Alternatively, you could detect whether it's Bash or not and include a Bash implementation and a POSIX one, but that seems like more effort than it's worth)

### `main` Function
Bash scripts should have their main logic contained in a `main` function. The final line of the file should call this function, like so:

```
main() {
    # Code goes here
}

main "$@"
```

This allows functions to be defined anywhere in the file and called. (It's also imo just a thing I like better)

<!-- References -->
[0]: https://gitlab.com/beewall/mc-crafting-villagers-addon/blob/master/DEVELOPMENT.md
   (This file on GitLab)